<pre>
<?php

class Tweet {}

$db = new mysqli('127.0.0.1', 'root', '', 'sait_twitter');

// SQL injection attack!!
$id = '1 or 1=1';

$result = $db->query('SELECT * FROM tweets');

$tweets = [];
foreach ($result as $row) {
  $tweet = new Tweet();
  $tweet->content = $row['content'];
  $tweet->date = date_create($row['date']);
  $tweets[] = $tweet;
}

var_dump( $tweets );
// var_dump( $result, $db->error );
