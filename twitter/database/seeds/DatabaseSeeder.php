<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\Profile;
use App\Models\Tweet;
use Faker\Factory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i=0; $i < 10; $i++) {
            $user = new User();
            $user->email = $faker->email;
            $user->name = $faker->name;
            $user->password = bcrypt('12341234');
            $user->save();

            $userProfile = new Profile();
            $userProfile->user_id = $user->id;
            $userProfile->image = 'https://picsum.photos/200/?random'.rand(1, 100);
            $userProfile->handle = $faker->word;
            $userProfile->description = $faker->name;
            $userProfile->website = $faker->url;
            $userProfile->save();

            $count = rand(2, 5);
            for ($j=0; $j < $count; $j++) {
                $tweet = new Tweet();
                $tweet->content = $faker->paragraph;
                $tweet->user_id = $user->id;
                $tweet->save();
            }
        }

        foreach (User::all() as $user) {
            foreach (Tweet::all() as $tweet) {
                $rand = rand(1, 100);
                if ($rand < 20) {
                    $user->likedTweets()->attach($tweet);
                }
            }
        }
    }
}
