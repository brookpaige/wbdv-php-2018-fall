<pre>
<?php

// numerically indexed array
$list = [
    'bob',
    'sue'
];

var_dump($list);
echo "\n";
echo json_encode($list);
echo "\n";

// associative array
$bob = [
    'name' => 'Bob',
    'age' => 21
];

var_dump($bob);
echo "\n";
// we can use JSON_PRETTY_PRINT to make
// it pretty
echo json_encode($bob, JSON_PRETTY_PRINT);

$bob['hairColor'] = 'brown';
if (!isset($bob['notThere'])) {
    echo 'NOT THERE';
}
echo "\n";
var_dump($bob);
echo "\n";
